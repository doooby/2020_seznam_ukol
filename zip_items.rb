
def zip_items items, sec_items_factory, offset, period
  required_count = 1 + (items.length - 1 - offset) / (period - 1)
  sec_items = sec_items_factory.(required_count)

  p_primary = 0
  p_secondary = 0
  i_period = 0
  result = Array.new items.length + required_count
  result.length.times do |i|
    if i < offset
      result[i] = items[i]
      next
    end

    if i_period == 0
      result[i] = sec_items[p_secondary]
      p_secondary += 1
      p_primary = offset + ( (p_secondary - 1) * (period - 1) )

    else
      result[i] = items[p_primary + i_period - 1]

    end

    i_period += 1
    if i_period == period
      i_period = 0
    end
  end

  result
end

def numbers_generator length
  Array.new(length){|i| i}
end

require 'json'
tests_json = File.read 'tests.json'
JSON.parse(tests_json).each do |src_count, offset, period, expected|
  puts "-------"
  items = numbers_generator(src_count).map{|n| 1000 + n }
  result = zip_items items, method(:numbers_generator), offset, period
  puts 'expected:'
  puts expected.to_s
  puts 'was:'
  puts result.to_s
  unless result == expected
    puts "FAIL"
    exit 1
  end
end

puts :OK
