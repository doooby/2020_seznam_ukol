
function zipItems(primaryItems, secondaryItemsFactory, offset, period) {
    const secondaryItemsCount = 1 + Math.floor(
        (primaryItems.length - 1 - offset) / (period - 1)
    );
    const secondaryItems = secondaryItemsFactory(secondaryItemsCount);

    let primaryPointer = 0, secondaryPointer = 0, periodIndex = 0;
    result = [];
    result.length = primaryItems.length + secondaryItemsCount;
    for (let i=0, len=result.length; i<len; i+=1) {
        if (i < offset) {
            result[i] = primaryItems[i];
            continue;
        }

        if (periodIndex === 0) {
            result[i] = secondaryItems[secondaryPointer];
            secondaryPointer += 1;
            primaryPointer = offset + ( (secondaryPointer - 1) * (period - 1) );

        } else {
            result[i] =  primaryItems[primaryPointer + periodIndex - 1];

        }

        periodIndex += 1;
        if (periodIndex === period) {
            periodIndex = 0;
        }
    }

    return result;
}

function numbersGenerator(length) {
    const arr = [];
    arr.length = length;
    for (let i=0, len=length; i<len; i+=1) {
        arr[i] = i;
    }
    return arr;
}

function areSameArrays(a, b) {
    if (a.length !== b.length) return false;
    for (let i=0, len=a.length; i<len; i+=1) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

const fs = require('fs');
const tests_json = fs.readFileSync('tests.json');
JSON.parse(tests_json).forEach((test_case) => {
    const [ src_count, offset, period, expected ] = test_case;
    console.log('--------');
    const items = numbersGenerator(src_count).map(n => 1000 + n);
    const result = zipItems(items, numbersGenerator, offset, period);
    console.log('expected:');
    console.log(expected.join(','));
    console.log('was:');
    console.log(result.join(','));
    if (!areSameArrays(expected, result)) {
        console.log('FAIL');
        process.exit(1);
    }
});

console.log('OK');
